package main.java;

import java.util.Scanner;

import static com.mongodb.client.model.Filters.eq;

public class Main {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Title:");

        String title = scanner.nextLine();

        Database.insertarPeliculaSQL(title);

        Database.insertarPeliculaMongo(title);
    }

}